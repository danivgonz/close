
const home = document.getElementById('home');
const features = document.getElementById('features');
const empresa = document.getElementById('empresa');

var header = document.getElementById("menu");


if (home || features || empresa) {
    window.onscroll = function() {myFunction()};
}

function myFunction() {
    if (window.pageYOffset > 10) {
        header.classList.add("is-active");
    } else {
        header.classList.remove("is-active");
    }
}

document.ready = function() {
    document.getElementsByClassName("menu-icon").click(function() {
        document.getElementsByClassName(".menu-content").classList.toggle("is-active");
    });
};
